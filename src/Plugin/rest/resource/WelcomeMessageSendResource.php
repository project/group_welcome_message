<?php

namespace Drupal\group_welcome_message\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountInterface;


/**
 * Represents Welcome Message Send records as resources.
 *
 * @RestResource (
 *   id = "group_welcome_message_welcome_message_send",
 *   label = @Translation("Welcome Message Send"),
 *   uri_paths = {
 *     "canonical" = "/api/group-welcome-message-welcome-message-send/{id}",
 *     "create" = "/api/group-welcome-message-welcome-message-send"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class WelcomeMessageSendResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * Entity type manger.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;  

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;  

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    EntityTypeManagerInterface $entity_type_manager,
    QueueFactory $queue_factory       
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory, $entity_type_manager);
    $this->storage = $keyValueFactory->get('group_welcome_message_welcome_message_send');
    $this->entityTypeManager = $entity_type_manager;
    $this->queue = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('entity_type.manager'),
      $container->get('queue')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param array $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data = ['error' => 'error']) {

    try {

      if (isset($data['error'])) {
        $this->logger->warning("Data can't be empty.");
        throw new NotFoundHttpException("Data can't be empty.");
      }

      if (isset($data['group']) && is_numeric($data['group']) && $data['group'] > 0) {
        
        // Check if we have a real group here.
        $group = $this->entityTypeManager->getStorage('group')->load($data['group']);

        if (!$group instanceof GroupInterface) {
          throw new NotFoundHttpException("There is no group for this group id."); 
        }

      }
      else {
        throw new NotFoundHttpException("Group can't be empty or null."); 
      }       

      if (isset($data['users']) && is_array($data['users']) && !empty($data['users'])) {


        foreach ($data['users'] as $user) {

          \Drupal::logger('debug')->debug('user id: ' . $user);

          $account = $this->entityTypeManager->getStorage('user')->load($user);

          if (!$account instanceof AccountInterface) {
            // Failure no valid user account
            throw new NotFoundHttpException("There was an invalid user detected."); 
          }
          else {

            if (!$this->isValidGroupMember($group, $account)) {
              throw new NotFoundHttpException("There was an invalid group member detected."); 
            }
          }

        }

        // Check if valid user ids
        $users = $this->entityTypeManager->getStorage('user')->loadMultiple($data['users']);
        if ($users && count($users) === count($data['users'])) {
          
          // Get the queue and save the users and group
          $queue = $this->queue->get('group_welcome_message_email_queue');
          $queue->createItem($data);  
          
          $data['status'] = 'success';
          $response = new ModifiedResourceResponse($data, 201);

        }

        
      }
      else {
        throw new NotFoundHttpException("Users can't be empty nor null."); 
      }   
    
    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      

    }
    catch(\EntityStorageException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
          
    }
    catch(\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);

    }   

    return $response;

  }

  /**
   * Get a group welcome message config entity
   * by it's group reference.
   */
  protected function isValidGroupMember(GroupInterface $group, AccountInterface $account) {


    // Get membership
    $membership = \Drupal::service('group.membership_loader')->load($group, $account);

    //\Drupal::logger('debug')->debug('<pre><code>' . print_r($membership, TRUE) . '</code></pre>');

    return $membership;

  }
 

}
