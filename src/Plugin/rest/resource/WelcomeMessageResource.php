<?php

namespace Drupal\group_welcome_message\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_welcome_message\Entity\GroupWelcomeMessageInterface;

/**
 * Represents Welcome message records as resources.
 *
 * @RestResource (
 *   id = "group_welcome_message_welcome_message",
 *   label = @Translation("Welcome message"),
 *   uri_paths = {
 *     "canonical" = "/api/group-welcome-message-welcome-message/{group}",
 *     "create" = "/api/group-welcome-message-welcome-message"
 *   }
 * )
 *
 * @DCG
 * The plugin exposes key-value records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. An
 * example of such configuration can be located in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can enable it through admin interface provider by REST UI
 * module.
 * @see https://www.drupal.org/project/restui
 *
 * @DCG
 * Notice that this plugin does not provide any validation for the data.
 * Consider creating custom normalizer to validate and normalize the incoming
 * data. It can be enabled in the plugin definition as follows.
 * @code
 *   serialization_class = "Drupal\foo\MyDataStructure",
 * @endcode
 *
 * @DCG
 * For entities, it is recommended to use REST resource plugin provided by
 * Drupal core.
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
class WelcomeMessageResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * Entity type manger.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    EntityTypeManagerInterface $entity_type_manager    
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory, $entity_type_manager);
    $this->storage = $keyValueFactory->get('group_welcome_message_welcome_message');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('entity_type.manager')      
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param array $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post(array $data = ['error' => 'error']) {

    $response = [];

    try {    

      if (isset($data['error'])) {
        $this->logger->warning("Data can't be empty.");
        throw new NotFoundHttpException("Data can't be empty.");
      }

      if (isset($data['group'])) {
        // We need to check the group id first
        $group = $this->entityTypeManager->getStorage('group')->load($data['group']);
        if ($group instanceof GroupInterface) {
          $group_uuid = $group->uuid();
        }
        else {
          // Failure
          throw new NotFoundHttpException("The given group parameter was corret, but no group has been found.");
        }
      }
      else {
        // Failure
        throw new NotFoundHttpException("The given parameter group was empty.");
      }

      // We need to check if we already have saved a
      // welcome message for this group
      if (!$this->getWelcomeMessageByGroup($data['group'])) {

        // We need to add our machine name for our config
        // entity.
        $data['id'] = 'welcome_message_' . $group_uuid;   

        $welcome_message = $this->entityTypeManager->getStorage('group_welcome_message')->create($data);
        //$this->checkEditFieldAccess($welcome_message);      
        //$this->validate($welcome_message);   
        $welcome_message->save();      

        $this->logger->notice($this->t("Welcome message with id @id saved!\n", ['@id' => $welcome_message->id()]));

        $response = new ModifiedResourceResponse($data, 201);

      }
      else {
        // Failure
        throw new NotFoundHttpException("There is already a welcome message defined for this group");
      }

    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      

    }
    catch(\EntityStorageException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
          
    }
    catch(\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);

    }   
    
    return $response;

  }

  /**
   * Responds to GET requests.
   *
   * @param int $group
   *   The group ID of the record.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   */
  public function get($group) {

    $response = [];
    $data = [];

    try {   

      if (isset($group) && is_numeric($group) && !empty($group) && $group > 0) {

        $group_welcome_message = $this->getWelcomeMessageByGroup($group);

        if ($group_welcome_message) {

          $data['data'] = $group_welcome_message;
          $data['status'] = 'success';

          $response = new ModifiedResourceResponse($data, 201);

        }
        else {
          throw new NotFoundHttpException("There was no welcome message found for this group.");
        }
      }
      else {
        throw new NotFoundHttpException("Missing group parameter.");
      }
    
    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $error['status'] = 'error';
      $response = new ModifiedResourceResponse($error, 400);      
  
    }
    catch(\EntityStorageException $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $error['status'] = 'error';
      $response = new ModifiedResourceResponse($error, 404);
            
    }
    catch(\Exception $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $error['status'] = 'error';
      $response = new ModifiedResourceResponse($error, 404);
  
    }    
    
    return $response;

  }

  /**
   * Responds to PATCH requests.
   *
   * @param int $group
   *   The group ID of the record.
   * @param array $data
   *   Data to write into the storage.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function patch($group, array $data = ['error' => 'error']) {


    try {

      if (isset($group) && is_numeric($group) && !empty($group) && $group > 0) {

        $group_welcome_message = $this->getWelcomeMessageByGroup($group);

        if (!$group_welcome_message) {
          throw new NotFoundHttpException("No welcome message found for this group.");
        }

      }
      else {
        throw new NotFoundHttpException("No valid group parameter.");
      }

      if ($data['error']) {

        $this->logger->warning("Data can't be empty.");
        throw new NotFoundHttpException("Data can't be empty.");

      }
      else {

        if (is_array($data) && !empty($data)) {
          
          foreach($data as $key => $value) {
            $group_welcome_message->set($key, $value);
          }

          $this->checkEditFieldAccess($group_welcome_message);      
          $this->validate($group_welcome_message);
          $group_welcome_message->save(); 

          $data['data'] = $group_welcome_message;
          $data['status'] = 'success';
          $response = new ModifiedResourceResponse($data, 201);          

        }
        else {

          $this->logger->warning("Data can't be empty.");
          throw new NotFoundHttpException("Data can't be empty.");

        }

      }

    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      
  
    }
    catch(\EntityStorageException $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
            
    }
    catch(\Exception $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
  
    }   
    
    return $response;

  }

  /**
   * Responds to DELETE requests.
   *
   * @param int $group
   *   The group ID of the record.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function delete($group) {

    try {

      if (isset($group) && is_numeric($group) && !empty($group) && $group > 0) {

        $group_welcome_message = $this->getWelcomeMessageByGroup($group);

        if (!$group_welcome_message) {
          throw new NotFoundHttpException("No welcome message found for this group.");
        }

      }
      else {
        throw new NotFoundHttpException("No valid group parameter.");
      }
      
      if ($group_welcome_message instanceof GroupWelcomeMessageInterface) {
        $group_welcome_message->delete();
        $response = new ModifiedResourceResponse(NULL, 204);         
      }

    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      
  
    }
    catch(\EntityStorageException $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
            
    }
    catch(\Exception $e) {
  
      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 404);
  
    } 
    
    return $response;

  }

  /**
   * Get a group welcome message config entity
   * by it's group reference.
   */
  protected function getWelcomeMessageByGroup(int $group) {

    $welcome_messages = $this->entityTypeManager->getStorage('group_welcome_message')->loadByProperties(['group' => $group]);
    
    if ($welcome_message = reset($welcome_messages)) {
      return $welcome_message;
    }
    else {
      return FALSE;
    }

  }


}
