<?php

namespace Drupal\group_welcome_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group_welcome_message\Entity\GroupWelcomeMessage;

/**
 * Group Welcome Message settings.
 */
class GroupWelcomeMessageSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_welcome_message_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'group_welcome_message.settings',
    ];
  }

  protected function getTranslatedConfigEntity(ConfigEntityInterface $configEntity, LanguageInterface $language) {
    $currentLanguage = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($language);
    $translatedConfigEntity = $this->storage
      ->getStorage($configEntity->getEntityTypeId())
      ->load($configEntity->id());
      $this->languageManager->setConfigOverrideLanguage($currentLanguage);

    return $translatedConfigEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {    

    $form = parent::buildForm($form, $form_state);
    $settings = $this->config('group_welcome_message.settings');

    // Get default message.
    $default_welcome_message = \Drupal::entityTypeManager()->getStorage('group_welcome_message')->load('default_group_welcome_message');
    if ($default_welcome_message) {      
      $default_subject = $default_welcome_message->getSubject();
      $default_body = $default_welcome_message->getBody();
      $default_body_existing = $default_welcome_message->getBodyExisting();
    }

    // Get existing values.
    $subject_label = $settings->get('subject_label');
    $body_label = $settings->get('body_label');
    $body_existing_label = $settings->get('body_existing_label');
    $selected_format = $settings->get('selected_format');    

    $available_filters = filter_formats();

    foreach($available_filters as $id => $filter) {
      $filter_options[$id] = $filter->label();
    }  

    $form['message'] = [
      '#type'          => 'fieldset',
      '#title'         => $this->t('Default message'),
      '#description'   => $this->t('If a default message is set here, and no message is set for the group, this default message will be sent.'),
    ];

    $form['message']['default_subject'] = [
      '#type' => 'textfield',
      '#title' => $subject_label,
      '#maxlength' => 255,
      '#default_value' => $default_subject,
    ];
 
    $form['message']['default_body'] = [
      '#type' => 'text_format',
      '#title' => $body_label,
      '#default_value' => $default_body['value'],
      '#format' => $selected_format,
      '#allowed_formats' => [
        $selected_format
      ]
    ];    

    $form['message']['default_body_existing'] = [
      '#type' => 'text_format',
      '#title' => $body_existing_label,
      '#default_value' => $default_body_existing['value'],
      '#required' => FALSE,
      '#format' => $selected_format,
      '#allowed_formats' => [
        $selected_format
      ]
    ];
    
    // This is currently not supported, 
    // because the default translation would not be used as a default translation 
    // for new group_welcome_messages using the default template, 
    // but only for groups having no group_welcome_message defined at all, 
    //leading to inconsistent behaviour.

    // $form['message']['translation'] = array(
    //   '#type' => 'html_tag',
    //   '#tag' => 'a',
    //   '#value' => $this->t("Translate default message"),
    //   '#attributes' => array(
    //     'class' => ["btn", "btn-primary", "button", "button--primary"],
    //     'href' => "/admin/group_welcome_message/default_group_welcome_message/edit/translate",
    //   ),
    // );

    $form['message']['available_tokens'] = array(
      '#type' => 'details',
      '#title' => t('Available Tokens'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    $supported_tokens = array('site','user','group');
    
    $available_field_tokens = \Drupal::service('group_welcome_message.available_fields');
    $whitelist = $available_field_tokens->getAvailableFields();

    $options = [
      'show_restricted' => TRUE,
      'show_nested' => TRUE,
      'global_types' => FALSE,
      'whitelist' => $whitelist
    ];  

    $form['message']['available_tokens']['tokens'] = \Drupal::service('group_welcome_message.tree_builder')
      ->buildRenderable($supported_tokens,$options);

    $form['labels'] = [
      '#type'          => 'fieldset',
      '#title'         => $this->t('Label and Format settings'),
    ];

    $form['labels']['selected_format'] = [
      '#title'         => $this->t('Message format'),
      '#type'          => 'select',
      '#options'       => $filter_options,
      '#default_value' => $settings->get('selected_format'),
    ];

    $form['labels']['subject_label'] = [
      '#title'         => $this->t('Define your own label for the subject field'),
      '#type'          => 'textfield',
      '#default_value' => !empty($subject_label) ? $subject_label : 'Subject',      
    ];

    $form['labels']['body_label'] = [
      '#title'         => $this->t('Define your own label for the body for new users field'),
      '#type'          => 'textfield',
      '#default_value' => !empty($body_label) ? $body_label : 'Body for new users',      
    ];

    $form['labels']['body_existing_label'] = [
      '#title'         => $this->t('Define your own label for the body for existing users field'),
      '#type'          => 'textfield',
      '#default_value' => !empty($body_existing_label) ? $body_existing_label : 'Body for existing users',      
    ];    

    $form['show_token_info'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Show token info'),
      '#default_value' => $settings->get('show_token_info'),
      '#description' => $this->t('If enabled a block with available tokens is shown to users.')
    ];
    

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory->getEditable('group_welcome_message.settings');

    // Save default welcome message.
    $default_welcome_message = \Drupal::entityTypeManager()->getStorage('group_welcome_message')->load('default_group_welcome_message');
    if (!$default_welcome_message) {        
      $default_welcome_message = GroupWelcomeMessage::create();
    }     

    $default_welcome_message->set('id', 'default_group_welcome_message');
    $default_welcome_message->set('label', 'default_group_welcome_message');
    $default_welcome_message->set('group', '0');
    $default_welcome_message->setSubject($form_state->getValue('default_subject'));
    $default_welcome_message->setBody($form_state->getValue('default_body'));
    $default_welcome_message->setBodyExisting($form_state->getValue('default_body_existing'));
    $default_welcome_message->save();    

    // Save rest of configuration.
    $settings->set('subject_label', $form_state->getValue('subject_label'))->save();
    $settings->set('body_label', $form_state->getValue('body_label'))->save();
    $settings->set('body_existing_label', $form_state->getValue('body_existing_label'))->save();
    $settings->set('selected_format', $form_state->getValue('selected_format'))->save();
    $settings->set('show_token_info', $form_state->getValue('show_token_info'))->save();

  }

}

